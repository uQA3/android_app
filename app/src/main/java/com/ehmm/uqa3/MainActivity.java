package com.ehmm.uqa3;

import android.app.DialogFragment;
import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.UnknownHostException;
import java.util.Random;

import io.github.controlwear.virtual.joystick.android.JoystickView;

public class MainActivity extends AppCompatActivity {

    private String mIp =  "224.0.0.14";
    private int mPort = 3333;

    private TextView mTVAngleLeft;
    private TextView mTVStrengthLeft;
    private TextView mTVThrottle;
    private TextView mTVYaw;

    private TextView mTVAngleRight;
    private TextView mTVStrengthRight;
    private TextView mTVPitch;
    private TextView mTVRoll;

    private double throttle = 50;
    private int old_throttle = 0;
    private double yaw = 0;
    private double pitch = 0;
    private double roll = 0;

    private Switch mSWConnect;
    private boolean connected = false;
    private UDP_Client Client;

    private boolean started = false;
    private Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (connected && old_throttle != (int) throttle)
            {
                old_throttle = (int) throttle;
                Log.e("stuff", "entering");
                JSONObject postData = new JSONObject();
                try {
                    postData.put("T", throttle);
                    postData.put("P", 0);
                    postData.put("Y", 0);
                    postData.put("R", 0);
                }catch (JSONException e){
                    e.printStackTrace();
                }
                Client.Message = postData.toString();
                // Client.Message = String.valueOf((int) throttle);
                //Send message
                Client.NachrichtSenden();
            }
            if(started) {
                start();
            }
        }
    };

    public void stop() {
        started = false;
        handler.removeCallbacks(runnable);
    }

    public void start() {
        started = true;
        handler.postDelayed(runnable, 50);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTVAngleLeft = findViewById(R.id.tv_angle_left);
        mTVStrengthLeft = findViewById(R.id.tv_strength_left);
        mTVThrottle = findViewById(R.id.tv_throttle);
        mTVYaw = findViewById(R.id.tv_yaw);

        JoystickView joystickLeft = findViewById(R.id.joystickView_left);

        joystickLeft.setFixedCenter(true);
        joystickLeft.setAutoReCenterButton(false);

        joystickLeft.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                throttle = Math.sin(angle*Math.PI/180)*strength;
                throttle = (int) ((throttle + 100) / 2);
                if (throttle > 100) throttle = 100;
                if (throttle < 5) throttle = 0;
                Log.e("THROTTLE", String.valueOf(throttle));
                yaw = Math.cos(angle*Math.PI/180)*strength;
                mTVAngleLeft.setText(angle + "°");
                mTVStrengthLeft.setText(strength + "%");
                mTVThrottle.setText("T: " + (int) throttle + "%");
                mTVYaw.setText("Y: " + (int) yaw + "%");
            }

        }, 500);



        mTVAngleRight = findViewById(R.id.tv_angle_right);
        mTVStrengthRight = findViewById(R.id.tv_strength_right);
        mTVPitch = findViewById(R.id.tv_pitch);
        mTVRoll = findViewById(R.id.tv_roll);

        JoystickView joystickRight = findViewById(R.id.joystickView_right);
        joystickRight.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                pitch = Math.sin(angle*Math.PI/180)*strength;
                roll = Math.cos(angle*Math.PI/180)*strength;
                mTVAngleRight.setText(angle + "°");
                mTVStrengthRight.setText(strength + "%");
                mTVPitch.setText("P: " + (int) pitch + "%");
                mTVRoll.setText("R: " + (int) roll + "%");
            }
        });

        mSWConnect = findViewById(R.id.sw_connect);
        mSWConnect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String msg = "";
                if ((int) throttle == 0)
                {
                    if (isChecked)
                    {
                        try {

                            Client = new UDP_Client(mIp, mPort);
                            connected = true;
                            start();
                            msg = "Conectado";
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }
                    }else
                    {
                        stop();
                        msg = "Desconectado";
                        mSWConnect.setChecked(false);
                        connected = false;
                    }
                }else
                {
                    msg = "Colocar throttle en cero para conectar/desconectar";
                    mSWConnect.setChecked(!isChecked);
                }
                Snackbar.make(findViewById(android.R.id.content),
                        msg, Snackbar.LENGTH_LONG).show();
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            DialogFragment dialog = new IPDialogFragment();
            dialog.show(getFragmentManager(), "IPDialogFragment");


            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onUserSelectValue(String ip, String port) {
        mIp = ip;
        mPort = Integer.valueOf(port);
    }
}
