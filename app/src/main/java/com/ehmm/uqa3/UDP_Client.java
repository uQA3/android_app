package com.ehmm.uqa3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by joakocero on 23/04/18.
 */


public class UDP_Client {


    InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        assert wifi != null;
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        Log.e("IP:", dhcp.ipAddress + " " + dhcp.netmask);
        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        Log.e("stuff", String.valueOf(InetAddress.getByAddress(quads)));
        return InetAddress.getByAddress(quads);
    }

    private AsyncTask<Void, Void, Void> async_cient;
    public String Message;
    public Context mContext;
    private String mIp;
    private int mPort;
    private InetAddress inetAddress;



    public UDP_Client(String ip, int port) throws UnknownHostException {
        mIp = ip;
        mPort = port;
        inetAddress = InetAddress.getByName(mIp); // 232.10.11.12  // 224.0.0.14 // 228.4.5.6
    }

    public static NetworkInterface findWifiNetworkInterface() {

        Enumeration<NetworkInterface> enumeration = null;

        try {
            enumeration = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        NetworkInterface wlan0 = null;

        while (enumeration.hasMoreElements()) {

            wlan0 = enumeration.nextElement();

            if (wlan0.getName().equals("wlan0")) {
                return wlan0;
            }
        }

        return null;
    }

    @SuppressLint({"NewApi", "StaticFieldLeak"})
    public void NachrichtSenden() {
        async_cient = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                //StrictMode.setThreadPolicy(policy);
                /*
                WifiManager wifi = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                DhcpInfo dhcp = wifi.getDhcpInfo();
                // handle null somehow

                Log.e("IP:", dhcp.ipAddress + " " + dhcp.netmask);
                WifiManager.MulticastLock multicastLock = wifi.createMulticastLock("multicastLock");
                multicastLock.setReferenceCounted(true);
                multicastLock.acquire();
                */
                DatagramSocket ds = null;

                try {
                    ds = new DatagramSocket();
                    /*
                    MulticastSocket multicastSocket = new MulticastSocket();

                    NetworkInterface wifiNetworkInterface = findWifiNetworkInterface();
                    if (wifiNetworkInterface != null){
                        multicastSocket.setNetworkInterface(wifiNetworkInterface);
                        Log.e("assadsad", "ENTROOO");
                    }
                    */
                    DatagramPacket dp;


                    dp = new DatagramPacket(Message.getBytes(),
                            Message.length(), inetAddress, mPort);
                    ds.setBroadcast(true);
                    ds.send(dp);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    //multicastLock.release();
                    if (ds != null) {
                        ds.close();
                    }
                }
                return null;
                /*
                WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
                WifiManager.MulticastLock lock = wifi.createMulticastLock("dk.aboaya.pingpong");
                lock.acquire();
                try{
                    DatagramSocket serverSocket = new DatagramSocket(3333);
                    DatagramPacket dp;
                    serverSocket.setBroadcast(true);

                    dp = new DatagramPacket(Message.getBytes(), 0,
                            Message.length(), getBroadcastAddress(), 3333);
                    serverSocket.send(dp);
                    lock.release();
                }catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (ds != null) {
                        ds.close();
                    }
                }
                return null;
*/



            }

            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
            }
        };

        if (Build.VERSION.SDK_INT >= 11)
            async_cient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else async_cient.execute();
    }
}
