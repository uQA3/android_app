package com.ehmm.uqa3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;

public class IPDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_ip, null))
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        EditText ip = getDialog().findViewById(R.id.ip);
                        EditText port = getDialog().findViewById(R.id.port);
                        String new_ip = ip.getText().toString();
                        if (new_ip.isEmpty()) new_ip = "224.0.0.14";
                        String new_port = port.getText().toString();
                        if (new_port.isEmpty()) new_port = "3333";
                        Log.d("IP: ", new_ip);
                        Log.d("PORT: ", new_port);
                        MainActivity callingActivity = (MainActivity) getActivity();
                        callingActivity.onUserSelectValue(new_ip, new_port);

                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        IPDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
